require_relative './reddit.rb'
require_relative './mashable.rb'
require_relative './digg.rb'
require_relative './all_for_one.rb'
require 'date'
require 'json'
require 'rest-client'
require 'launchy'

class Start
  attr_accessor :all, :a, :k
  def initialize
    @reddit = Reddit.new
    @reddit.reddit_viewer
    @mashable = Mashable.new
    @mashable.mashable_viewer
    @digger = Digg.new
    @digger.digg_viewer
    @forone=Allforone.new
    @a=[]
    @k=[]
    @all={}
  end
  def rss
    @forone.fusion
    @forone.fusion_sorter
    @forone.fusion_pager
  end
  def menu
    loop do
      system("clear")
      puts "1- Mostrar noticias por sitio web"
      puts "2- Ver las noticias por fecha de publicacion"
      puts "3- Salir"
      opc1 = gets.to_i
      case opc1
      when 1
        system("clear")
        puts "1- Si desea ver Noticias de Reddit"
        puts "2- Si desea ver Noticias de Mashable"
        puts "3- Si desea ver Noticias de Digg"
        puts "4- Si desea volver a Menu Anterior"
        opc2 = gets.to_i
        case opc2
        when 1
          system("clear")
          @reddit.reddit_pager
        when 2
          system("clear")
          @mashable.mash_pager
        when 3
          system("clear")
          @digger.digg_pager
        else
        end
      when 2
        system("clear")
        rss
      else
      end
    break if (opc1 == 3)
    end
  end
end
m = Start.new
m.menu
