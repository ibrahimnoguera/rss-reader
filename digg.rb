require 'rest-client'
require 'json'
require 'date'
require_relative 'news'

class Digg < News
  def digg_viewer
    parser
    @news3[:data][:feed].each_with_index do |x, num=0| { x => num+=1 }
      if x[:content][:author] == "" then x[:content][:author] = "Anonimo" end
      @digg[num.to_s.to_sym]={:title => x[:content][:title_alt], :author => x[:content][:author], :url => x[:content][:url], :date => (Time.at(x[:date]).strftime("%d/%m/%Y %I:%M %p")), :source => "Digg"}
    end
    @all3=@digg
  end
  def url_opener
    puts "Para ir al enlace, presione el numero, de lo contrario presione cualquier letra"
    opc = gets.chomp.to_i
    if @a.include?(opc) then
      Launchy.open(@digg[@a[opc-1].to_s.to_sym][:url])
    else
    end
  end
  def all_for_one
    @all2=@digg
  end
  def digg_pager
    @k=@digg.keys
    i=0
    loop do
      system("clear")
      for j in i...i+5 do
        puts "Noticia #{@k[j].to_s.to_i}"
        puts "Titulo #{@digg[@k[j]][:title]}"
        puts "Autor: #{@digg[@k[j]][:author]}"
        puts "Direccion de la noticia : #{@digg[@k[j]][:url]}"
        puts "Fecha de creacion: #{@digg[@k[j]][:date]}"
        puts " "
      end
      @a=(i+1..i+5).to_a
      puts @a.to_s
      url_opener
      puts "Anterior: a, Siguiente: d"
      opc = gets.chomp
      if i==0 && opc == "a" then
        i = 0
      elsif i+1>=@k.length-5 then
        i = @k.length
      elsif opc == "a" then
        i-=5
      elsif opc == "d" then
        i+=5
      else
        puts "Opcion incorrecta"
      end
      break if (i>=@k.length)
    end
  end
end
