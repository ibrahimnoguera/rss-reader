require 'rest-client'
require 'json'
require 'date'
require_relative 'news'

class Reddit < News
  def reddit_viewer
    parser
    @news1[:data][:children].each_with_index do |x, num=0| { x => num+=1 }
      @red[num.to_s.to_sym]={:title => x[:data][:title], :author => x[:data][:author], :url => "https://www.reddit.com#{x[:data][:permalink]}", :date => (Time.at(x[:data][:created]).strftime("%d/%m/%Y %I:%M %p")), :source => "Reddit"}
    end
    @all1=@red
  end
  def url_opener
    puts "Para ir al enlace, presione el numero, de lo contrario presione cualquier letra"
    opc = gets.chomp.to_i
    if @a.include?(opc) then
      Launchy.open(@red[@a[opc-1].to_s.to_sym][:url])
    else
    end
  end
  def reddit_pager
    @k=@red.keys
    i=0
    loop do
      system("clear")
      for j in i...i+5 do
        puts "Noticia #{@k[j].to_s.to_i}"
        puts "Titulo #{@red[@k[j]][:title]}"
        puts "Autor: #{@red[@k[j]][:author]}"
        puts "Direccion de la noticia : #{@red[@k[j]][:url]}"
        puts "Fecha de creacion: #{@red[@k[j]][:date]}"
        puts " "
      end
      @a=(i+1..i+5).to_a
      puts @a.to_s
      url_opener
      puts "Anterior: a, Siguiente: d"
      opc = gets.chomp
      if i==0 && opc == "a" then
        i = 0
      elsif i+1>=@k.length-5 then
        i = @k.length
      elsif opc == "a" then
        i-=5
      elsif opc == "d" then
        i+=5
      else
        puts "Opcion incorrecta"
      end
      break if (i>=@k.length)
    end
  end
end
