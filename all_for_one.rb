require_relative './reddit.rb'
require_relative './mashable.rb'
require_relative './digg.rb'

class Allforone < News
  attr_accessor :all, :k, :a
  def initialize
    @redd= Reddit.new
    @masha = Mashable.new
    @digge = Digg.new
    @all={}
    @k=[]
    @a=[]
  end
  def fusion
    @all1=@redd.reddit_viewer
    @all2=@masha.mashable_viewer
    @all3=@digge.digg_viewer
    l=@all1.length+@all2.length+@all3.length
    l.times do |x=0| {x => x+=1}
      @all[x.to_s.to_sym]=nil
    end
    @all1.length.times do |x=0| {x => x+=1}
      if @all[x.to_s.to_sym]==nil
        @all[x.to_s.to_sym]=@all1[x.to_s.to_sym]
      end
    end
    num=@all1.length
    @all2.each do |k, v|
      num+=1
      @all[num.to_s.to_sym]=@all2[k]
    end
    num=@all1.length+@all2.length
    @all3.each do |k, v|
      num+=1
      @all[num.to_s.to_sym]=@all3[k]
    end
  end
  def fusion_sorter
    puts @all.sort_by { |k, v| v[:date] }.reverse
    puts "Paginando..."
    sleep 5
  end
  def url_opener
    puts "Para ir al enlace, presione el numero, de lo contrario presione cualquier letra"
    opc = gets.chomp.to_i
    if @a.include?(opc) then
      Launchy.open(@all[@a[opc-1].to_s.to_sym][:url])
    else
    end
  end
  def fusion_pager
    system('clear')
    @k=@all.keys
    i=0
    loop do
      system("clear")
      for j in i...i+5 do
        puts "Noticia #{@k[j].to_s.to_i}"
        puts "Titulo #{@all[@k[j]][:title]}"
        puts "Autor: #{@all[@k[j]][:author]}"
        puts "Direccion de la noticia : #{@all[@k[j]][:url]}"
        puts "Fecha de creacion: #{@all[@k[j]][:date]}"
        puts "Fuente: #{@all[@k[j]][:source]}"
        puts " "
      end
      @a=(i+1..i+5).to_a
      puts @a.to_s
      url_opener
      puts "Anterior: a, Siguiente: d"
      opc = gets.chomp
      if i==0 && opc == "a" then
        i = 0
      elsif i+1>=@k.length-6 then
        i = @k.length
      elsif opc == "a" then
        i-=5
      elsif opc == "d" then
        i+=5
      else
        puts "Opcion incorrecta"
      end
      break if (i>=@k.length)
    end
  end
end
