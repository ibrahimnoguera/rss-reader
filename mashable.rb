require 'rest-client'
require 'json'
require 'date'
require_relative 'news'

class Mashable < News
  def mashable_viewer
    parser
    @news2["new".to_sym].each_with_index do |x, num=0| { x => num+=1 }
      @mash[num.to_s.to_sym]={:title => x[:title], :author => x[:author], :url => x[:link], :date => (Time.at(DateTime.parse(x[:post_date]).to_time.to_i).strftime("%d/%m/%Y %I:%M %p")), :source => "Mashable", :section => "New"}
    end
    num2=@mash.length
    @news2["rising".to_sym].each do |x|
      num2+=1
      @mash[num2.to_s.to_sym]={:title => x[:title], :author => x[:author], :url => x[:link], :date => (Time.at(DateTime.parse(x[:post_date]).to_time.to_i).strftime("%d/%m/%Y %I:%M %p")), :source => "Mashable", :section => "Rising"}
    end
    num2=@mash.length
    @news2["hot".to_sym].each do |x|
      num2+=1
      @mash[num2.to_s.to_sym]={:title => x[:title], :author => x[:author], :url => x[:link], :date => (Time.at(DateTime.parse(x[:post_date]).to_time.to_i).strftime("%d/%m/%Y %I:%M %p")), :source => "Mashable", :section => "Hot"}
    end
    @all2=@mash
  end
  def url_opener
    puts "Para ir al enlace, presione el numero, de lo contrario presione cualquier letra"
    opc = gets.chomp.to_i
    if @a.include?(opc) then
      Launchy.open(@mash[@a[opc-1].to_s.to_sym][:url])
    else
    end
  end
  def mash_pager
    @k=@mash.keys
    puts @k
    i=0
    loop do
      system("clear")
      for j in i...i+5 do
        puts "Noticia #{@k[j].to_s.to_i}"
        puts "Titulo #{@mash[@k[j]][:title]}"
        puts "Autor: #{@mash[@k[j]][:author]}"
        puts "Direccion de la noticia : #{@mash[@k[j]][:url]}"
        puts "Fecha de creacion: #{@mash[@k[j]][:date]}"
        puts "Seccion: #{@mash[@k[j]][:source]}"
        puts " "
      end
      @a=(i+1..i+5).to_a
      puts @a.to_s
      url_opener
      puts "Anterior: a, Siguiente: d"
      puts i
      opc = gets.chomp
      if i==0 && opc == "a" then
        i = 0
      elsif i+1>=@k.length-6 then
        i = @k.length
      elsif opc == "a" then
        i-=5
      elsif opc == "d" then
        i+=5
      else
        puts "Opcion incorrecta"
      end
      break if (i>=@k.length)
    end
  end
end
