class News
  attr_accessor :redrss, :mashrss, :diggrss, :news1, :news2, :news3, :all1, :all2, :all3
  def initialize
    @mashrss = "https://www.mashable.com/stories.json"
    @diggrss = "digg.com/api/news/popular.json"
    @redrss = "https://www.reddit.com/.json"
    @mash = {}
    @digg = {}
    @red={}
    @all1={}
    @all2={}
    @all3={}
  end
  def parser
    @news1 = JSON.parse(RestClient.get(@redrss), :symbolize_names => true )
    @news2 = JSON.parse(RestClient.get(@mashrss), :symbolize_names => true )
    @news3 = JSON.parse(RestClient.get(@diggrss), :symbolize_names => true )
  end
end
